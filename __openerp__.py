# -*- coding: utf-8 -*-
{
    'name': "Dynamic Read Only Example",

    'summary': "Demonstration for read only field based on a dynamic condition",

    'description': """
        Demonstration for read only field based on a dynamic condition

        In this example a user should be able to update a field only if current time is between two time intervals

        For Example :

        Editable From = 01:30

        Editable To = 08:30

        If the current time is 05:20 the field should be editable, but if the current time is 14:40 the field should be frozen
    """,

    'author': "Emad Shaaban",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr'],

    # always loaded
    'data': [
        'views.xml'
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}
