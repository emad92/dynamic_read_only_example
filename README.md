# Demonstration for read only field based on a dynamic condition


In this example a user should be able to update a field only if current time is between two time intervals

## For Example :

    Editable From = 01:30

    Editable To = 08:30

If the current time is 05:20 the field should be editable, but if the current time is 14:40 the field should be frozen
