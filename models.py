# -*- coding: utf-8 -*-
import datetime

from openerp import models, fields, api

class m4d_gv_fup_goals(models.Model):
    _name = 'm4d_gv_fup.goals'

    editable_from = fields.Float(string="Editable From", default=0)
    editable_to = fields.Float(string="Editable To", default=24)

    goal_owner = fields.Many2one(string='Goal Owner', comodel_name='hr.employee',
                                 index='True')
    my_subs_activities = fields.One2many('m4d_gv_fup.goals_act', inverse_name='goal',
                                         string="My Subs Activities")

    editable = fields.Boolean(compute="_compute_editable")

    @api.multi
    @api.depends('editable_from', 'editable_to')
    def _compute_editable(self):
        current_time = datetime.datetime.now().time()
        current_time_as_float = current_time.hour + current_time.minute / 60.0
        for record in self:
            record.editable = record.editable_from < current_time_as_float < record.editable_to


class m4d_gv_fup_goals_activities(models.Model):
    _name = 'm4d_gv_fup.goals_act'

    owned_by = fields.Many2one(
        string="Owned By", comodel_name='hr.employee', store=True)

    goal = fields.Many2one(
        string='For Goal', comodel_name='m4d_gv_fup.goals', index='True')
